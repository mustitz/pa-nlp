#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from distutils.core import setup

setupArgs = {
    'name'         : 'パ NLP',
    'version'      : '0.1',
    'description'  : 'Python library Natural Language Processing (custom tasks not covered by another libs).',
    'author'       : 'Andrii Sevastianov',
    'author_email' : 'mustitz@gmail.com',
    'license'      : 'MIT',
    'packages'     : ['pa']
}

setup(**setupArgs)
