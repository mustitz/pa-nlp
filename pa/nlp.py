from pymystem3 import Mystem

def ideal_case(word, base_splitted):
    root = ''.join(base_splitted[:-1])
    if word.startswith(root):
        return '/'.join(base_splitted[:-1] + [word[len(root):]])
    return '?'

def verb_tsia_case(word, base_splitted):
    if base_splitted[-2:] == ['ть', 'ся']:
        root = ''.join(base_splitted[:-2])
        if word.startswith(root):
            return '/'.join(base_splitted[:-2] + [word[len(root):]])
    return '?'

def soft_sign_case(word, base_splitted):
    root = ''.join(base_splitted[:-1])
    if root[-1] == 'ь':
        root = root[:-1]
        if word.startswith(root):
            return '/'.join(base_splitted[:-1] + ['%', word[len(root):]])
    return '?'

class Morphemer:
    def __init__(self, morphemes):
        self.replacement = { row.word.replace('ё', 'е'): row.morphemes.replace('ё', 'е') for row in morphemes.itertuples() if row.mean == 0 }
        self.mystem = Mystem()

    def try_split_to_morphemes(self, word):
        base = self.mystem.lemmatize(word)[0]
        base_splitted = self.replacement.get(base, '?')
        if base_splitted == '?':
            return '?'
        base_splitted = base_splitted.split('/')
        if len(base_splitted) <= 1:
            return '?'

        for rule in [ideal_case, verb_tsia_case, soft_sign_case]:
            result = rule(word, base_splitted[:])
            if result != '?':
                return result

        return '?'
